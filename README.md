Jibber
======

Jibber Jabber, a conversation simulation program. A sort of uber-Eliza. Made on a Linux platform.

It uses a SQLite database of phrases, trigger key words and other required response strings as a sort of weak 'knowledge base' and a simplified reasoning engine to create output in response to the user's input.

Requires the development libraries and toolkits for SQLite and GTK+3 to compile & link, and uses Glade3 for the UI design.

Compile with:
gcc -Wall -c -g "%f" `pkg-config --cflags gtk+-3.0`

Build with:
gcc -Wall -g -o "%e" "%f" -lsqlite3 `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0 gmodule-2.0`

a makefile is supplied.


