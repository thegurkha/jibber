/*
 * add_data.c
 *
 * Copyright 2013 Dave McKay dmckay@btconnect.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <sqlite3.h>

// database handle
sqlite3 *knowledgeDB = NULL;

int ReadInPhrasesFile( void );
int PostKeyWord(char *KeyWord, int KeyWordGroup);
int PostCategory(char *Category, int KeyWordGroup);
int PostResponse(char *Response, int KeyWordGroup);
int PostViewpoint(char *Viewpoint);
int PostPostprocessing(char *Postprocess);

#define STAND_ALONE						0
#define INTEGRATED						1

#define NULL_BYTE							0x00
#define RETURN								0x0D
#define NEW_LINE							0x0A
#define HASH									'#'
#define SPACE									' '
#define NEW_KEYWORD						'='
#define NEW_GROUP_INDICATOR		'*'
#define NEW_SHIFT_INDICATOR		'@'
#define NEW_POSTP_INDICATOR		'!'
#define CATEGORY_TITLE				'$'

const char INSERT_RESPONSE[] = "INSERT INTO phrases VALUES ('%s', \"%s\", '%s', '%s')";
const char INSERT_KEYWORD[] = "INSERT INTO triggers VALUES (\"%s\", '%d')";
const char INSERT_CATEGORY[] = "INSERT INTO categories VALUES (\"%s\", '%d')";
const char INSERT_VIEWPOINT[] = "INSERT INTO viewpoint VALUES (\"%s\", \"%s\")";
const char INSERT_POSTPROCESSING[] = "INSERT INTO postprocessing VALUES (\"%s\", \"%s\")";

int main(int argc, char **argv)
{

	char *pError;
	int nResponse;

	// intialise the SQLite library
	sqlite3_initialize( );

	// open the database
	nResponse = sqlite3_open_v2( "worldview.sl3", &knowledgeDB, SQLITE_OPEN_READWRITE, NULL );

	// if it didn't open OK, shutdown and exit
	if ( nResponse != SQLITE_OK) {
		printf("%s Couldn't open world view database.\n", argv[0]);
		sqlite3_close( knowledgeDB );
		exit (-1);
	}
	else
			printf("Opened world view database OK\n");

	nResponse = sqlite3_exec(knowledgeDB, "DELETE from phrases", NULL, NULL, &pError);

	if ( nResponse != SQLITE_OK ) {
		printf("Couldn't delete old responses (phrases).\n");
		return (-1);
	}
	else
			printf("Zapped old responses (phrases).\n");

	nResponse = sqlite3_exec(knowledgeDB, "DELETE from triggers", NULL, NULL, &pError);

	if ( nResponse != SQLITE_OK ) {
		printf("Couldn't delete old key words (triggers).\n");
		return (-1);
	}
	else
			printf("Zapped old key words (triggers).\n");

	nResponse = sqlite3_exec(knowledgeDB, "DELETE from categories", NULL, NULL, &pError);

	if ( nResponse != SQLITE_OK ) {
		printf("Couldn't delete old categories (categories).\n");
		return (-1);
	}
	else
			printf("Zapped old categories (categories).\n");

	nResponse = sqlite3_exec(knowledgeDB, "DELETE from viewpoint", NULL, NULL, &pError);

	if ( nResponse != SQLITE_OK ) {
		printf("Couldn't delete old grammatical shifts (viewpoint).\n");
		return (-1);
	}
	else
			printf("Zapped old grammatical shifts (viewpoint).\n");

	nResponse = sqlite3_exec(knowledgeDB, "DELETE from postprocessing", NULL, NULL, &pError);

	if ( nResponse != SQLITE_OK ) {
		printf("Couldn't delete old post-processing shifts (postprocessing).\n");
		return (-1);
	}
	else
			printf("Zapped old post-processing shifts (postprocessing).\n");

	ReadInPhrasesFile();

	// shut down and exit
	sqlite3_close( knowledgeDB );
	sqlite3_shutdown( );

	return (0);

}	// end of main

int ReadInPhrasesFile( void )
{
	char szBuffer[ 1024 ];
	int FirstCharacter, CurrentKeyWordGroup = 1;
	int nResponse=0;

	// file handle
	FILE *InFile;

	// open the file
	InFile=fopen("phrases.jj", "rt");

	// did it open OK
	if (InFile == NULL) {
		printf("Can't open phrases file.\n");
		return (1);
	}

    // read it line by line until we get to the end of the file
    while (fgets(szBuffer, 1023, InFile) != NULL) {

		// the first character of the line dictates what kind of line it is
		FirstCharacter = szBuffer[0];

		// if it is a comment ignore it
		if (FirstCharacter == HASH)
				continue;

		if (FirstCharacter == SPACE)
				continue;

		// if it isn't an asterisk, remove trailing line feeds etc
		if ( FirstCharacter != NEW_GROUP_INDICATOR )
				szBuffer[ strlen( szBuffer ) - 2 ] = NULL_BYTE;
		else
				CurrentKeyWordGroup++;

		// is the first character an = sign?
		if ( FirstCharacter == NEW_KEYWORD ) {

			// yes, add a new trigger phrase to this group
			PostKeyWord(szBuffer+1, CurrentKeyWordGroup);
		}
		// is the first character a $ sign?
		else if ( FirstCharacter == CATEGORY_TITLE ) {

			// yes, add a new category to the database
			PostCategory(szBuffer+1, CurrentKeyWordGroup);
		}
		// is the first character either a 1 or a 0?
		else if ( isdigit( FirstCharacter ) ) {

			// yes, add a new response to this group
			PostResponse(szBuffer, CurrentKeyWordGroup);
		}
		// is the first character an @ sign?
		else if ( FirstCharacter == NEW_SHIFT_INDICATOR ) {

			// yes, add a new first / third person viewpoint pairing
			PostViewpoint(szBuffer+1);
		}
		// is the first character an ! sign?
		else if ( FirstCharacter == NEW_POSTP_INDICATOR ) {

			// yes, add a new first / third person post-processing pairing
			PostPostprocessing(szBuffer+1);
		}
	}

	return ( nResponse );

}   // ReadInPhrasesFile

int PostResponse(char *Response, int KeyWordGroup)
{
	char *Ptr, szSeps[]=";\n";
	char szType[ 5 ], szResponse[ 1024 ], szCloser[ 5 ], szCategory[ 5 ];
	char szSQLStatement[1024];
	char *pError;

	int nResponse;

	// extract the response type
	Ptr = strtok(Response, szSeps);

	// if we got it OK, save it for later
	if (Ptr != NULL)
		sprintf(szType, "%s", Ptr);
	else {
		// flag error
		printf("Can't extract Type from response phrase\n");
		return (-1);
	}

	// extract the phrase
	Ptr = strtok(NULL, szSeps);

	// if we got it ok, save it for later
	if (Ptr != NULL)
		sprintf(szResponse, "%s", Ptr);
	else {
		// flag error
		printf("Can't extract Phrase from response phrase\n");
		return (-1);
	}

	// extract the closing punctuation
	Ptr = strtok(NULL, szSeps);

	// if we got it ok, save it for later
	if (Ptr != NULL)
		sprintf(szCloser, "%s", Ptr);
	else {
		// flag error
		printf("Can't extract Closer from response phrase\n");
		return (-1);
	}

	// store the category number for later
	sprintf(szCategory, "%d", KeyWordGroup);

	// compose the SQL string
	sprintf(szSQLStatement, INSERT_RESPONSE, szType, szResponse, szCloser, szCategory);

	puts( szSQLStatement );

	nResponse = sqlite3_exec(knowledgeDB, szSQLStatement, NULL, NULL, &pError);

	if ( nResponse != SQLITE_OK ) {
		printf("Couldn't add that response (phrases).\n");
		return (-1);
	}

	return (0);

}	// end of PostResponse

int PostKeyWord(char *KeyWord, int KeyWordGroup)
{
	char szSQLStatement[1024];
	char *pError;

	int nResponse;

	// compose the SQL string
	sprintf(szSQLStatement, INSERT_KEYWORD, KeyWord, KeyWordGroup);

	puts( szSQLStatement );

	nResponse = sqlite3_exec(knowledgeDB, szSQLStatement, NULL, NULL, &pError);

	if ( nResponse != SQLITE_OK ) {
		printf("Couldn't add that keyword (triggers).\n");
		return (-1);
	}

	return (0);

}	// end of PostKeyWord

int PostCategory(char *Category, int KeyWordGroup)
{
	char szSQLStatement[1024];
	char *pError;

	int nResponse;

	// compose the SQL string
	sprintf(szSQLStatement, INSERT_CATEGORY, Category, KeyWordGroup);

	puts( szSQLStatement );

	nResponse = sqlite3_exec(knowledgeDB, szSQLStatement, NULL, NULL, &pError);

	if ( nResponse != SQLITE_OK ) {
		printf("Couldn't add that category.\n");
		return (-1);
	}

	return (0);

}	// end of PostCategory

int PostViewpoint(char *Viewpoint)
{
	char *Ptr, szSeps[]=",;\n";
	char szFirst[ 15 ], szThird[ 15 ];
	char szSQLStatement[1024];
	char *pError;

	int nResponse;

	// extract the first person
	Ptr = strtok(Viewpoint, szSeps);

	// if we got it OK, save it for later
	if (Ptr != NULL)
		sprintf(szFirst, "%s", Ptr);
	else {
		// flag error
		printf("Can't extract first person viewpoint from viewpoint phrase\n");
		return (-1);
	}

	// extract the third person
	Ptr = strtok(NULL, szSeps);

	// if we got it ok, save it for later
	if (Ptr != NULL)
		sprintf(szThird, "%s", Ptr);
	else {
		// flag error
		printf("Can't extract third person viewpoint from viewpoint phrase\n");
		return (-1);
	}

	// compose the SQL string
	sprintf(szSQLStatement, INSERT_VIEWPOINT, szFirst, szThird);

	puts( szSQLStatement );

	nResponse = sqlite3_exec(knowledgeDB, szSQLStatement, NULL, NULL, &pError);

	if ( nResponse != SQLITE_OK ) {
		printf("Couldn't add that first/third person viewpoint (viewpoint).\n");
		return (-1);
	}

	return (0);

}	// end of PostViewpoint

int PostPostprocessing(char *Postprocess)
{
	char *Ptr, szSeps[]=",;\n";
	char szBad[ 15 ], szGood[ 15 ];
	char szSQLStatement[1024];
	char *pError;

	int nResponse;

	// extract the first person
	Ptr = strtok(Postprocess, szSeps);

	// if we got it OK, save it for later
	if (Ptr != NULL)
		sprintf(szBad, "%s", Ptr);
	else {
		// flag error
		printf("Can't extract bad pairing from post-process phrase\n");
		return (-1);
	}

	// extract the third person
	Ptr = strtok(NULL, szSeps);

	// if we got it ok, save it for later
	if (Ptr != NULL)
		sprintf(szGood, "%s", Ptr);
	else {
		// flag error
		printf("Can't extract good pairing from post-process phrase\n");
		return (-1);
	}

	// compose the SQL string
	sprintf(szSQLStatement, INSERT_POSTPROCESSING, szBad, szGood);

	puts( szSQLStatement );

	nResponse = sqlite3_exec(knowledgeDB, szSQLStatement, NULL, NULL, &pError);

	if ( nResponse != SQLITE_OK ) {
		printf("Couldn't add that good / bad pairing in post-processing (postprocess).\n");
		return (-1);
	}

	return (0);

}	// end of PostPostprocess
