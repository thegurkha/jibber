/*
 * function_headers.h
 *
 * Copyright 2013-14 Dave McKay <dmckay@btconnect.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

// function headers
void GetReplyFromJJ(char *InputText, int nMode, JJGui *jibber_ui);
int GetKeyWordGroup( char *szInputText, int *nTailOffset, JJGui *jibber_ui);
int SetToUppercase(char *Text);
void GetResponseFromGroup(int KeyWordGroup, char *szUserInput, int nTailOffset, JJGui *jibber_ui);
int GetGroupFromTriggerPhrase( char *TriggerPhrase, JJGui *jibber_ui);
int GetPhrasesCount(int nkeyWordGroup);
void FirstPersonShiftTail( char *szShiftedTail );
void PostProcessShiftTail( char *szShiftedTail );
void FinalProcessShiftTail( char *szShiftedTail );

void AppendText(JJGui *jibber_ui, int nDirection, char *NewString, ... );
void WriteFile(JJGui *jibber_ui);

struct Tlink *AddTopicToList(int KeyWordGroup, char *pTriggerPhrase, char *pUserInput, JJGui *jibber_ui);
void DoListWalk(JJGui *jibber_ui);
void DeleteAllTopics(void);
void DeleteEndTopic(void);
int GetTopicUsage(int nKeyWordGroup);
char *GetCategoryFromKeyGroup(int nKeyWordGroup);
int CheckForProcessedCategories(int nKeyWordGroup);
void GetResponseFromProcessGroup(int KeyWordGroup, char *szUserInput, JJGui *jibber_ui);
void PerformAboutCommand( JJGui *jibber_ui );

G_MODULE_EXPORT void on_window_destroy(GObject *object, JJGui *jibber_ui);
G_MODULE_EXPORT void on_button1_clicked(GObject *object, JJGui *jibber_ui);
G_MODULE_EXPORT void on_text_field_activate(GObject *object, JJGui *jibber_ui);
