/*
 * gtk_functions.h
 *
 * Copyright 2013 Dave McKay <dmckay@btconnect.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <gtk/gtk.h>

#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#include <sqlite3.h>

#include "jibber_defines.h"
#include "function_headers.h"

G_MODULE_EXPORT void on_window_destroy(GObject *object, JJGui *jibber_ui)
{
	// quit the GUI loop
	gtk_main_quit();

}	// end of on_window_destroy

G_MODULE_EXPORT void on_button1_clicked(GObject *object, JJGui *jibber_ui)
{
	const gchar *text;

	char *UserInput;

	// user has hit the post button or the enter key, retrieve the text from the text entry field
	text = gtk_entry_get_text(GTK_ENTRY(jibber_ui->edit_field));

	// take a copy of the string and process it
	UserInput = strdup(text);
	GetReplyFromJJ(UserInput, NORMAL_MODE, jibber_ui);

	// clear out the text entry field buffer
	gtk_entry_set_text(GTK_ENTRY(jibber_ui->edit_field), "");

	free(UserInput);

}	// end of on_button1_clicked

G_MODULE_EXPORT void on_text_field_activate(GObject *object, JJGui *jibber_ui)
{
	// user hit enter, let's process their input
	on_button1_clicked(object, jibber_ui );

}	// end of on_text_field_button_press_event

/*
G_MODULE_EXPORT void autoscroll_to_end( GtkWidget* widget, GtkAllocation* allocation, JJGui *jibber_ui)
{
	GtkAdjustment	*vericalAdjust;
	GtkAllocation	hscrollAlloc;
	GtkWidget		*hscrollBar;
	double			upper, height;

	// makes sure the bottom row is always visible in the window

	// get the vertical adjustment of the scrolled window
	vericalAdjust = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(widget));

	// get the upper bound of the adjustment - you want your text to end at that point
	upper = gtk_adjustment_get_upper( vericalAdjust );
	height = allocation->height;

	// if there is a scroll bar at the bottom, it will steal some of the available pixels for the text
	hscrollBar = gtk_scrolled_window_get_hscrollbar( GTK_SCROLLED_WINDOW(widget));
	gtk_widget_get_allocation(hscrollBar, &hscrollAlloc);

	// as long as (the scroll widget height - height of the horiz scrollbar) is the same as the upper bound of
	// the adjustment, the whole text can be seen without obstruction and you do not need to auto scroll
	if ((height - hscrollAlloc.height) >= upper) {
		gtk_adjustment_set_value(vericalAdjust, 0);
	}
	else {
		// otherwise you need to scroll down to the point where the bottom line is visible
		double newPosition = upper - (height - hscrollAlloc.height);
		gtk_adjustment_set_value(vericalAdjust, newPosition);
	}

} 	// end of autoscroll_to_end
*/
