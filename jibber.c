/*
 * jibber.c
 *
 * Copyright 2013-14 Dave McKay <dmckay@btconnect.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option)any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
#include <gtk/gtk.h>

#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#include <sqlite3.h>

#define MAIN

#include "jibber_defines.h"
#include "function_headers.h"
#include "jibber_globals.h"
#include "jibber_commands.h"

int main(int argc, char **argv)
{
	int 							nResponse, i, nKeyWordGroup = 0;
	GtkTextBuffer			*buffer;
	GtkBuilder 				*builder;
	JJGui							*jibber_ui;

	// allocate the memory needed by our structure
	jibber_ui = g_slice_new(JJGui);

	// init the gtk toolkit
	gtk_init(&argc, &argv);

	// get a new interface builder
	builder = gtk_builder_new();

	// read the GUI definitions from the XML file
	gtk_builder_add_from_file(builder, "jibber.glade", NULL);

	// point the structure members to the UI elements we need to control
	jibber_ui->window = GTK_WIDGET (gtk_builder_get_object (builder, "window"));
	jibber_ui->edit_field = GTK_ENTRY (gtk_builder_get_object (builder, "text_field"));
	jibber_ui->text_view = GTK_WIDGET (gtk_builder_get_object (builder, "textview1"));

	// connect the signal handlers
	gtk_builder_connect_signals(builder, jibber_ui);

	// release the builder memory
	g_object_unref(G_OBJECT (builder));

	// get the style settings from the CSS file
	GtkCssProvider *cssProvider = gtk_css_provider_new();
  gtk_css_provider_load_from_path(cssProvider, "themes.css", NULL);
  gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(cssProvider), GTK_STYLE_PROVIDER_PRIORITY_USER);

	// get a handle on the buffer from the text window
	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW (jibber_ui->text_view));

	// create some style tags
	gtk_text_buffer_create_tag(buffer, "italic", "style", PANGO_STYLE_ITALIC, NULL);
	gtk_text_buffer_create_tag(buffer, "bold", "weight", PANGO_WEIGHT_BOLD, NULL);

	// intialise the SQLite library
	sqlite3_initialize();

	// Initialize random number generator
	srand(time(NULL));

	// don't show the internal workings
	bShow = false;

	// open the database
	nResponse = sqlite3_open_v2("worldview.sl3", &knowledgeDB, SQLITE_OPEN_READWRITE, NULL);

	// if it didn't open OK, shutdown and exit
	if (nResponse != SQLITE_OK) {
		printf("Couldn't open the World View database [worldview.sl3].\n");
		sqlite3_close(knowledgeDB);
		exit (-1);
	}

	// set all the position flags to 'never been used'
	for (i=0; i<1000; i++)
			nGroupPositionFlags[i] = (-2);

	// record the KeyWordGroups for the categories that need special processing - time specific
	nProcessCategories[TIME_HANDLER] = GetGroupFromTriggerPhrase("WHAT TIME IS IT", jibber_ui);
	nProcessCategories[DATE_HANDLER] = GetGroupFromTriggerPhrase("WHAT DATE IS IT", jibber_ui);
	nProcessCategories[DAY_HANDLER] = GetGroupFromTriggerPhrase("WHAT DAY IS IT", jibber_ui);
	nProcessCategories[YEAR_HANDLER] = GetGroupFromTriggerPhrase("WHAT YEAR IS IT", jibber_ui);
	nProcessCategories[MONTH_HANDLER] = GetGroupFromTriggerPhrase("WHAT MONTH IS IT", jibber_ui);
	nProcessCategories[END_MARKER] = (-1);

	// no topics in the linked list yet
	Root = End = NULL;

	// Announce ourselves
	AppendText(jibber_ui, DIR_SYSTEM, "Jibber Jabber %d.%d.%d :: Compiled on %s at %s.",
			JJ_VERSION_MAJOR, JJ_VERSION_MINOR, JJ_VERSION_BUILD, __DATE__, __TIME__);
	AppendText(jibber_ui, DIR_BLANK, "");

	nKeyWordGroup = GetGroupFromTriggerPhrase("JJOPENINGLINES", jibber_ui);

	// get response from this keywordgroup
	if (nKeyWordGroup)
			GetResponseFromGroup(nKeyWordGroup, "", 0, jibber_ui);

	// display the window
	gtk_widget_show(jibber_ui->window);

	// enter the UI event handler loop
	gtk_main();

	// shut down and exit
	sqlite3_close(knowledgeDB);
	sqlite3_shutdown();

	// flag exit code to OS
	return (0);

}	// end of main

void GetReplyFromJJ(char *InputText, int nMode, JJGui *jibber_ui)
{
	static char szUserInput[512], szLastInput[512];
	char *cPtr, szSeps[]=" \n";
	int  i, nResponse, nKeyWordGroup = 0, nTailOffset = 0, nCommand = (-1);

	// take a working copy of the string
	strcpy(szUserInput, InputText);

	// get the first word of the user input
	cPtr = strtok(InputText, szSeps);

	if (cPtr != NULL) {

		// see if the first word is in the list of known commands
		for (i=0; dCommands[i].Command != NULL; i++) {

			// check abbreviations and full command names
			if ((strcmp(dCommands[i].Abbrev, cPtr)== 0)|| (strcmp(dCommands[i].Command, cPtr)== 0)) {
				// a command has been found - get its number
				nCommand = dCommands[i].nCommand;
				break;
			}
		}

		// according to the number of the found command, do something
		if (nCommand >= 0) {

			switch (nCommand) {
				// is it a command string?
				case COMMAND_TRON:
					// turn on the system trace function
					bShow = true;
					AppendText(jibber_ui, DIR_SYSTEM, "Process trace ON");
					break;

				case COMMAND_TROFF:
					// turn off the system trace function
					bShow = false;
					AppendText(jibber_ui, DIR_SYSTEM, "Process trace OFF");
					break;

				case COMMAND_SAVE:
					// save the conversation to a file
					WriteFile(jibber_ui);
					break;

				case COMMAND_TOPICS:
					// show the topics used so far in the current conversation
					DoListWalk(jibber_ui);
					break;

				case COMMAND_QUIT:
				case COMMAND_EXIT:
					// leave the program
					gtk_widget_destroy(jibber_ui->window);
					break;

				case COMMAND_HELP:
					// display a help text
					AppendText(jibber_ui, DIR_SYSTEM, "Jibber Jabber %d.%d.%d :: Compiled on %s at %s.",
							JJ_VERSION_MAJOR, JJ_VERSION_MINOR, JJ_VERSION_BUILD, __DATE__, __TIME__);
					AppendText(jibber_ui, DIR_BLANK, "");

					for (i=0; dCommands[i].Command != NULL; i++)
							AppendText(jibber_ui, DIR_SYSTEM, "%-10s - %s", dCommands[i].Help1, dCommands[i].Help2);

					AppendText(jibber_ui, DIR_BLANK, "");
					break;

				case COMMAND_ABOUT:
					PerformAboutCommand(jibber_ui);
					break;

				// unknown command - do nothing
				default : break;
			}

			// we have executed a command, no further processing required
			return;
		}
	}

	if (nMode == NORMAL_MODE)
			AppendText(jibber_ui, DIR_IN, szUserInput);

	// force user input to uppercase
	SetToUppercase(szUserInput);

	// no user entry - pick a null input response
	if (strlen(szUserInput)== 0)
			nKeyWordGroup = GetGroupFromTriggerPhrase("JJNULLINPUT", jibber_ui);

	// same user input as last time - pick a repeat response
	else if (strcmp(szUserInput, szLastInput)== 0)
			nKeyWordGroup = GetGroupFromTriggerPhrase("JJREPEATEDINPUT", jibber_ui);

	// check it for keywords
	else {
		// take a copy for next time
		strcpy(szLastInput, szUserInput);

		// determine the keyword group from the content of the user entry
		nKeyWordGroup = GetKeyWordGroup(szUserInput, &nTailOffset, jibber_ui);
	}

	// did we find a keyword group? If we didn't, do further processing
	if (!nKeyWordGroup) {

		if (Root != NULL) {

			if (bShow) {

				AppendText(jibber_ui, DIR_SYSTEM, "Re-using previous seed statement: %s", End->UserInput);
			}

			//
			// also needs to be able to choose a new topic
			// instead of battering away at one recent topic
			//

			// re-use the last topic group
			GetReplyFromJJ(End->UserInput, SILENT_MODE, jibber_ui);
		}
		else {

			// if there is nothing to refer back to use a non-specific response
			nKeyWordGroup = GetGroupFromTriggerPhrase("JJNONSPECIFIC", jibber_ui);
		}
	}

	if (!nKeyWordGroup)
			return;

	// AppendText(jibber_ui, DIR_SYSTEM, "group: %d", nKeyWordGroup);

	// Check whether it is one of the specially handled categories, requiring
	// specific processing. If it is, deal with it. If not, handle it as normal
	nResponse = CheckForProcessedCategories(nKeyWordGroup);

	// get response from this keyword group
	if (!nResponse)
			GetResponseFromGroup(nKeyWordGroup, szUserInput, nTailOffset, jibber_ui);
	else
			GetResponseFromProcessGroup(nKeyWordGroup, szUserInput, jibber_ui);

}	// end of GetReplyFromJJ

int GetKeyWordGroup(char *szInputText, int *nTailOffset, JJGui *jibber_ui)
{
	int KeyWordGroup = 0, TriggerLength, nResponse;
	char *TriggerLocation = NULL, *Tail, *TriggerPhrase;
	char szSQLString[50]="SELECT * FROM triggers";

	bool bFound = false, bWholeWord = false, bBefore, bAfter;

	sqlite3_stmt *stmt = NULL;

	*nTailOffset = 0;

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(knowledgeDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
		return (-1);

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	// as long as we have a valid record set row
	while (nResponse == SQLITE_ROW) {

		// get the data from the record
		TriggerPhrase = (char*)sqlite3_column_text(stmt, KEYPHRASE);

		// if the trigger phrase is in this sentence TriggerLocation will not be NULL
		TriggerLocation = strstr(szInputText, TriggerPhrase);

		if (TriggerLocation != NULL) {

			// get the length of the trigger phrase
			TriggerLength = strlen(TriggerPhrase);

			// was it found at the beginning of the user input?
			if (TriggerLocation == szInputText) {

				// what is the next character after the potential found trigger?
				bWholeWord = !(isalpha(*(TriggerLocation+TriggerLength)));
			}
			else {

				// what are the next and preceding characters around the potential found trigger?
				bBefore = isalpha(*(TriggerLocation-1));
				bAfter = isalpha(*(TriggerLocation+TriggerLength));

				// is this a word within a word or a valid triggger phrase?
				bWholeWord = (!bBefore && !bAfter)? true : false;
			}

			// if we have found a trigger phrase
			if (bWholeWord) {

				if (bShow) {
					AppendText(jibber_ui, DIR_SYSTEM, "Triggered on: %s", TriggerPhrase);
				}

				// something valid was found
				bFound = true;

				// get the group field
				KeyWordGroup = sqlite3_column_int(stmt, CATEGORY);

				// store this as a topic
				AddTopicToList(KeyWordGroup, TriggerPhrase, szInputText, jibber_ui);

				// point the tail pointer to the remainder of the sentence
				Tail = TriggerLocation;

				// if the remainder is the same length as the actual trigger phrase
				// then there is no tail, otherwise return the tail offset
				*nTailOffset = ((int)strlen(Tail)== (int)TriggerLength)? 0 : (TriggerLocation + TriggerLength)- szInputText;

				if ((bShow)&& (nTailOffset)) {

					AppendText(jibber_ui, DIR_SYSTEM, "Tail: %s", TriggerLocation + TriggerLength);
				}
			}
			else {
				*nTailOffset = 0;
			}
		}

		// found something, stop looking
		if (bFound)
				nResponse = SQLITE_ERROR;
		else
				// get next record
				nResponse = sqlite3_step(stmt);
	}

	sqlite3_finalize(stmt);

	// send back the keywordgroup or 0 for nothing found
	return (bFound)? KeyWordGroup : 0;

}	// end of GetKeyWordGroup

int GetGroupFromTriggerPhrase(char *TriggerPhrase, JJGui *jibber_ui)
{
	char szTemplate[]="SELECT * FROM triggers WHERE keyphrase=\"%s\"";
	char szSQLString[100]="", szTrigger[40]="";

	int nResponse, nKeyWordGroup = 0;

	sqlite3_stmt *stmt = NULL;

	// take a working copy of the trigger phrase and make it upper case
	strcpy(szTrigger, TriggerPhrase);
	SetToUppercase(szTrigger);

	// create the SQL string
	sprintf(szSQLString, szTemplate, szTrigger);

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(knowledgeDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
			return (0);

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	// if there was one valid row, get the keyword group
	while (nResponse == SQLITE_ROW) {

		// take a copy of the key word group numerical indicator
		nKeyWordGroup = atoi((char*)sqlite3_column_text(stmt, CATEGORY));

		if (bShow) {

			AppendText(jibber_ui, DIR_SYSTEM, "Triggered on: %s", TriggerPhrase);
		}

		// force a break from the loop
		nResponse = SQLITE_ERROR;
	}

	sqlite3_finalize(stmt);

	// send it back
	return (nKeyWordGroup);

}	// end of GetGroupFromTriggerPhrase

int GetPhrasesCount(int nKeyWordGroup)
{
	char szCountTemplate[]="SELECT count(*)FROM phrases WHERE category=%d";
	char szSQLString[50]="";
	int nResponse, nRecordCount;

	sqlite3_stmt *stmt = NULL;

	// form the SQL string
	sprintf(szSQLString, szCountTemplate, nKeyWordGroup);

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(knowledgeDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
			return (0);

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	if (nResponse == SQLITE_ROW)
			nRecordCount = sqlite3_column_int(stmt, TYPE);

	sqlite3_finalize(stmt);

	return (nRecordCount);

}	// end of GetPhrasesCount

void GetResponseFromGroup(int KeyWordGroup, char *szUserInput, int nTailOffset, JJGui *jibber_ui)
{
	char szTemplate[]="SELECT * FROM phrases WHERE category=%d";
	char szShiftedTail[150] = "";
	char szSQLString[50]="";
	char szCloser[10], szPhrase[350], szFinalResponse[512] = "";
	int nResponse, nType, nRecordCount, nPosition, nLoop = 0;

	bool bFound = false;

	sqlite3_stmt *stmt = NULL;

	// how many response phrases in this keyword group
	nRecordCount = GetPhrasesCount(KeyWordGroup);

	// which one did we use last time
	nPosition = nGroupPositionFlags[KeyWordGroup];

	// if we have never used a response from this group, randomly set the first one to use
	if (nPosition == (-2))
			nPosition = rand()% nRecordCount;
	else {
		++nPosition;
		nPosition = (nPosition >= nRecordCount)? 0 : nPosition;
	}

	if (bShow) {
		AppendText(jibber_ui, DIR_SYSTEM, "Group: %d, Resp: %d, Last: %d", KeyWordGroup, nRecordCount, nPosition);
	}

	// store the new position so that next time we know which one we used last
	nGroupPositionFlags[KeyWordGroup] = nPosition;

	// form the SQL string
	sprintf(szSQLString, szTemplate, KeyWordGroup);

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(knowledgeDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
			return;

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	// as long as we have a valid record set row
	while (nResponse == SQLITE_ROW) {

		// if this record row is one more than the one last used
		if (nLoop == nPosition) {

			bFound = true;

			// get the type, phrase and the end punctuation
			nType  = (int)sqlite3_column_int(stmt, TYPE);
			strcpy(szPhrase, (char*)sqlite3_column_text(stmt, PHRASE));
			strcpy(szCloser, (char *)sqlite3_column_text(stmt, CLOSER));
		}

		// we found the response we were looking for
		if (bFound)
				nResponse = SQLITE_ERROR;
		else {

			++nLoop;
			// step through the loop to the next record
			nResponse = sqlite3_step(stmt);
		}
	}

	sqlite3_finalize(stmt);

	// do we need to make an integrated response?
	if ((nType == 1)&& (nTailOffset != 0)) {

		// yes, and there is a tail available so compose the integrated response
		strcpy(szShiftedTail, (szUserInput+nTailOffset));

		// check for changes required for change of first person perspective
		FirstPersonShiftTail(szShiftedTail);

		// compose the final response
		sprintf(szFinalResponse, "%s%s%s", szPhrase, szShiftedTail, szCloser);
	}
	else {
		// standalone response
		sprintf(szFinalResponse, "%s%s", szPhrase, szCloser);
	}

	// finally, display the response from JJ to the user
	AppendText(jibber_ui, DIR_OUT, szFinalResponse);

}	// end of GetResponseFromGroup

void FirstPersonShiftTail(char *szShiftedTail)
{
	char Seps[]=" .-:,!?\n";
	char szWorkingTail[512]=" ";
	char szSQLString[]="SELECT * FROM viewpoint";
	char *Ptr;

	int nResponse;

	bool bFound;

	sqlite3_stmt *stmt = NULL;

	// try to get the first word
	Ptr = strtok(szShiftedTail, Seps);

	// nothing of value in the tail
	if (Ptr == NULL) {

		// send back a blank tail
		strcpy(szShiftedTail, "");
		return;
	}

	// loop through the tail looking for words and checking if
	// they need swapping to accomodate the new first person shift
	while (Ptr != NULL) {

		// prepare the SQL statement
		nResponse = sqlite3_prepare_v2(knowledgeDB, szSQLString, -1, &stmt, NULL);

		// if not OK, bomb out
		if (nResponse != SQLITE_OK)
				return;

		// if OK, start to walk through the record set
		nResponse = sqlite3_step(stmt);

		bFound = false;

		// as long as we have a valid record set row
		while (nResponse == SQLITE_ROW) {

			// if we have found a word in the phrase that needs converting to a new viewpoint
			if (strcmp(Ptr, (char *)sqlite3_column_text(stmt, FIRSTPERSON))== 0) {

				// swap it with the viewpoint shifted equivalent
				strcat(szWorkingTail, (char *)sqlite3_column_text(stmt, THIRDPERSON));
				// flag we have found one
				bFound = true;
				// break out of the loop
				nResponse = SQLITE_ERROR;
			}
			else
					// move on to the next word
					nResponse = sqlite3_step(stmt);
		}

		sqlite3_finalize(stmt);

		// if no substition took place, use the original word
		if (!bFound)
				strcat(szWorkingTail, Ptr);

		// look for the next word in the tail
		Ptr = strtok(NULL, Seps);

		// another word was found to check, so add a trailing space
		if (Ptr != NULL)
				strcat(szWorkingTail, " ");
	}

	// finally, copy the shifted tail back into the original
	strcpy(szShiftedTail, szWorkingTail);

	// post-process check for bad grammar in person-shifted tail
	PostProcessShiftTail(szShiftedTail);

}	// end of FirstPersonShiftedTail

void PostProcessShiftTail(char *szShiftedTail)
{
	char szSQLString[]="SELECT * FROM postprocessing";
	char *TriggerLocation = NULL, *BadPhrase,*GoodPhrase;
	char szNewTail[512];
	int TriggerLength, nResponse;

	bool bValidPhrase = false, bBefore, bAfter;

	sqlite3_stmt *stmt = NULL;

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(knowledgeDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
		return;

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	// as long as we have a valid record set row
	while (nResponse == SQLITE_ROW) {

		// get the data from the record
		BadPhrase = (char*)sqlite3_column_text(stmt, BADPAIRING);

		// if the trigger phrase is in this sentence TriggerLocation will not be NULL
		TriggerLocation = strstr(szShiftedTail, BadPhrase);

		if (TriggerLocation != NULL) {

			// get the length of the trigger phrase
			TriggerLength = strlen(BadPhrase);

			// was it found at the beginning of the user input?
			if (TriggerLocation == szShiftedTail) {

				// what is the next character after the potential found trigger?
				bValidPhrase = !(isalpha(*(TriggerLocation+TriggerLength)));
			}
			else {

				// what are the next and preceding characters around the potential found trigger?
				bBefore = isalpha(*(TriggerLocation-1));
				bAfter = isalpha(*(TriggerLocation+TriggerLength));

				// is this a word within a word or a valid triggger phrase?
				bValidPhrase = (!bBefore && !bAfter)? true : false;
			}

			// if we have found a trigger phrase
			if (bValidPhrase) {

				// get the data from the record
				GoodPhrase = (char*)sqlite3_column_text(stmt, GOODPAIRING);

				// stick a null byte in the middle of the tail to terminate it in front of the trigger phrase
				*(TriggerLocation-1)= 0x00;

				// copy into the tail the first part before the trigger, the new
				// good phrase and the remainder from behind the old bad phrase
				sprintf(szNewTail, "%s %s %s", szShiftedTail, GoodPhrase, TriggerLocation+TriggerLength);
				strcpy(szShiftedTail, szNewTail);
			}
		}

		// get next record
		nResponse = sqlite3_step(stmt);
	}

	if (szShiftedTail[strlen(szShiftedTail)-1] == 0x20)
			szShiftedTail[strlen(szShiftedTail)-1] = 0x00;

	FinalProcessShiftTail(szShiftedTail);

}	// end of PostProcessShiftTail

void FinalProcessShiftTail(char *szShiftedTail)
{
	char *TriggerLocation = NULL, AM_Phrase[10] = "I AM ", *ARE_Phrase = "ARE";
	char szNewTail[512];
	int TriggerLength, i, j;

	bool bValidPhrase = false, bBefore, bAfter;

	// if the phrase is in this sentence TriggerLocation will not be NULL
	TriggerLocation = strstr(szShiftedTail, AM_Phrase);

	// if we have found a trigger phrase
	if (!TriggerLocation) {

		strcpy(AM_Phrase, "AM");
		TriggerLength = strlen(AM_Phrase);

		// if the phrase is in this sentence TriggerLocation will not be NULL
		TriggerLocation = strstr(szShiftedTail, AM_Phrase);

		while (TriggerLocation) {

			// what are the next and preceding characters around the potential found trigger?
			bBefore = isalpha(*(TriggerLocation-1));
			bAfter = isalpha(*(TriggerLocation+TriggerLength));

			// is this a word within a word or a valid triggger phrase?
			bValidPhrase = (!bBefore && !bAfter)? true : false;

			if (bValidPhrase) {

				// stick a null byte in the middle of the tail to terminate it in front of the trigger phrase
				*(TriggerLocation-1)= 0x00;

				// copy into the tail the first part before the trigger, the new
				// ARE phrase and the remainder from behind the old AM phrase
				sprintf(szNewTail, "%s %s %s", szShiftedTail, ARE_Phrase, TriggerLocation+TriggerLength);

				strcpy(szShiftedTail, szNewTail);
				// check if there are other AM phrases in the tail that need processing
				TriggerLocation = strstr(szShiftedTail, AM_Phrase);
			}
			else
					TriggerLocation = NULL;
		}
	}

	// trim off trailings spaces
	if (szShiftedTail[strlen(szShiftedTail)-1] == 0x20)
			szShiftedTail[strlen(szShiftedTail)-1] = 0x00;

	// run through the string to make sure it has no double spacing in it
	for (i=0, j=0; szShiftedTail[i] != 0x00; i++) {

		// transfer the nextcharacter
		szNewTail[j] = szShiftedTail[i];

		// if the newly transferred character was a space and the next one is a space don't increment j
		j = ((szShiftedTail[i] == 0x20)&& (szShiftedTail[i+1] == 0x20))? j : j+1;
	}

	// cap the string
	szNewTail[j] = 0x00;

	// copy it back into the szShiftedTail string
	strcpy(szShiftedTail, szNewTail);

}	// end of FinalProcessShiftTail

int SetToUppercase(char *Text)
{
	int i;

	// force string letter by letter to uppercase
	for (i=0; Text[i] != 0x00; i++) {

		Text[i]=toupper(Text[i]);
	}

	return (0);

}	// end of SetToUppercase

void AppendText(JJGui *jibber_ui, int nDirection, char *NewString, ...)
{
	GtkTextBuffer *buffer;
	GtkTextIter iter;
	char text[250];
	char cPrefix;

	const gchar *tag_name;

	char TextBuffer[225];
	va_list argptr;

	// if required compose the multiple arguments into a single line of text
	if (NewString != NULL) {
		va_start(argptr, NewString);
		vsprintf(TextBuffer, NewString, argptr);
		va_end(argptr);
	}

	// determine the line prefix character and font attributes
	// according to the type of message to display
	switch (nDirection) {

		// the user said something
		case DIR_IN: cPrefix = '<';
				tag_name = "bold";
				break;

		// jibber jabber said something
		case DIR_OUT: cPrefix = '>';
				tag_name = NULL;
				break;

		// system trace message
		case DIR_SYSTEM: cPrefix = '+';
				tag_name = "italic";
				break;

		// for a blank line
		case DIR_BLANK: cPrefix = ' ';
				tag_name = NULL;
				break;

		// if any other message type is requested show a !
		default: cPrefix = '!';
				tag_name = NULL;
				break;
	}

	// compose the line of text to append to the buffer
	sprintf(text, "%c %s\n", cPrefix, TextBuffer);

	// get a handle on the buffer from the text window
	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW (jibber_ui->text_view));

	// get the location of the last character + 1
	gtk_text_buffer_get_end_iter(buffer, &iter);

	// add a mark to the buffer, and place it at the end of the buffer
	GtkTextMark *gTextMark = gtk_text_mark_new(NULL, FALSE);
	gtk_text_buffer_add_mark(buffer, gTextMark, &iter);

	// display the line of text by adding it to the buffer
	gtk_text_buffer_insert_with_tags_by_name(buffer, &iter, text, -1, tag_name, NULL);

	// scroll the window to the new mark (the end fo the buffer), then delete the mark
	gtk_text_view_scroll_to_mark(GTK_TEXT_VIEW (jibber_ui->text_view), gTextMark, 0.0, FALSE, 0.0, 0.0);
	gtk_text_buffer_delete_mark(buffer, gTextMark);

}	// end of AppendText

void WriteFile(JJGui *jibber_ui)
{
	GError					*err = NULL;
	gchar						*text;
	gboolean				result;
	GtkTextBuffer		*buffer;
	GtkTextIter			start, end;
	char						szFilename[35] = "";

	time_t t;
	struct tm *tmNow;

	// make sure the gui is finished with any queued instructions before going further
	while (gtk_events_pending())
			gtk_main_iteration();

	// get contents of buffer
	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW (jibber_ui->text_view));
	gtk_text_buffer_get_start_iter(buffer, &start);
	gtk_text_buffer_get_end_iter(buffer, &end);
	text = gtk_text_buffer_get_text(buffer, &start, &end, FALSE);
	gtk_text_buffer_set_modified(buffer, FALSE);

	t = time(NULL);
	tmNow = localtime(&t);

	if (tmNow == NULL) {

		// create a filename from the compile stamp
		sprintf(szFilename, "jibber_%s_%s.txt", __DATE__, __TIME__);
	}
	else {

		// create a filename from the time
		strftime(szFilename, 35, "jibber_%F_%H-%M-%S.txt", tmNow);
	}

	result = g_file_set_contents (szFilename, text, -1, &err);

	if (result == FALSE) {

		// error saving file, show message to user
		AppendText(jibber_ui, DIR_SYSTEM, "Couldn't save file: %s", szFilename);
		AppendText(jibber_ui, DIR_SYSTEM, (err->message));
	}
	else
			AppendText(jibber_ui, DIR_SYSTEM, "Saved file: %s", szFilename);

	// free the text memory
	g_free (text);

}	// end of WriteFile

struct Tlink *AddTopicToList(int KeyWordGroup, char *pTriggerPhrase, char *pUserInput, JJGui *jibber_ui)
{
	struct Tlink *fPtr;

	// no topics in the list yet
	if (Root == NULL) {

		// get memory for the new topic structure
		fPtr = (struct Tlink *)malloc(sizeof(struct Tlink));

		// can't allocate memory
		if (fPtr == NULL) {
			AppendText(jibber_ui, DIR_SYSTEM, "Error: Can't allocate memory for topic: %s in linked list.", pTriggerPhrase);
			return (NULL);
		}

		// set this topic as the start of the linked list
		Root = End = fPtr;
	}
	// topics exist, append new topic to the list
	else {
		// get memory for the new topic structure
		fPtr = (struct Tlink *)malloc(sizeof(struct Tlink));

		// can't allocate memory
		if (fPtr == NULL) {

			AppendText(jibber_ui, DIR_SYSTEM, "Error: Can't allocate memory for topic: %s in linked list.", pTriggerPhrase);
			return (NULL);
		}

		// point the hitherto last topic to the new last topic
		End->Next=fPtr;

		// this is now the end of the linked list
		End=fPtr;
	}

	// populate the topic field
	fPtr->TriggerPhrase = strdup(pTriggerPhrase);

	// can't allocate memory
	if (fPtr->TriggerPhrase == NULL) {

		AppendText(jibber_ui, DIR_SYSTEM, "Error: Can't allocate memory for topic: %s in linked list.", pTriggerPhrase);
		return (NULL);
	}

	// populate the user input field
	fPtr->UserInput = strdup(pUserInput);

	// can't allocate memory
	if (fPtr->UserInput == NULL) {

		AppendText(jibber_ui, DIR_SYSTEM, "Error: Can't allocate memory for topic: %s in linked list.", pTriggerPhrase);
		return (NULL);
	}

	// populate the category name field
	fPtr->CategoryName = strdup(GetCategoryFromKeyGroup(KeyWordGroup));

	// can't allocate memory
	if (fPtr->CategoryName == NULL) {

		AppendText(jibber_ui, DIR_SYSTEM, "Error: Can't allocate memory for topic: %s in linked list.", pTriggerPhrase);
		return (NULL);
	}
	// store the response group
	fPtr->nKeyWordGroup = KeyWordGroup;

	// nothing comes after this link
	fPtr->Next=NULL;

	if (bShow) {
		AppendText(jibber_ui, DIR_SYSTEM, "Added topic: %s, [%03d]", fPtr->TriggerPhrase, fPtr->nKeyWordGroup);
		AppendText(jibber_ui, DIR_SYSTEM, "Category Type: %s", fPtr->CategoryName);
	}

	// return from function
	return (fPtr);

}	// end of AddTopicToList

void DoListWalk(JJGui *jibber_ui)
{
	struct Tlink *fPtr;

	if (Root == NULL) {
		AppendText(jibber_ui, DIR_SYSTEM, "No topics in the topic list.");
		return;
	}

	// From the start of the linked list (root), display all topics
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

		AppendText(jibber_ui, DIR_OUT, "[%03d] '%s', '%s', '%s'",
			fPtr->nKeyWordGroup,
			fPtr->CategoryName,
			fPtr->TriggerPhrase,
			fPtr->UserInput);
	}

}	// end of DoListWalk

void DeleteAllTopics(void)
{
	struct Tlink *fPtr = NULL, *nPtr = NULL;

	// From the start of the linked list (root), delete all topics
	for (fPtr=Root; fPtr != NULL; fPtr=nPtr) {

		// get the pointer to the next topic before we delete this one
		nPtr = fPtr->Next;

		// free up the memory for the strdup'd strings
		free(fPtr->TriggerPhrase);
		free(fPtr->UserInput);
		free(fPtr->CategoryName);

		// null the pointer - safe but not strictly required
		fPtr->Next = NULL;

		// free the memory for the structure itself
		free(fPtr);
	}

	// set start and end of the linked list to null
	// to indicate that the linked list is empty
	Root = End = NULL;

}	// end of DeleteAllTopics

void DeleteEndTopic(void)
{
	struct Tlink *fPtr = NULL, *nPtr = NULL;

	// no topics?
	if (Root == NULL)
			return;

	// if there is only one topic
	if (Root->Next == NULL) {

		// free up the memory for the strdup'd strings
		free(Root->TriggerPhrase);
		free(Root->UserInput);
		free(Root->CategoryName);

		// null the pointer - safe but not strictly required
		Root->Next = NULL;

		// free the memory for the structure itself
		free(Root);

		Root = End = NULL;

		return;
	}

	// if we are here there must be more than one topic

	// Find the last item in the topic list
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next)
			nPtr= fPtr;

	// run through the list until we are the next to last topic
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

		// if the next topic is the last topic, we are at the new last topic
		if (fPtr->Next == nPtr) {

			// set fptr as the new last topic
			fPtr->Next = NULL;
			End = fPtr;

			// free up the memory for the strdup'd strings of the old last topic
			free(nPtr->TriggerPhrase);
			free(nPtr->UserInput);
			free(nPtr->CategoryName);

			// null the pointer - safe but not strictly required
			nPtr->Next = NULL;

			// free the memory for the structure itself
			free(nPtr);

			break;
		}
	}

}	// end of DeleteEndTopic

int GetTopicUsage(int nKeyWordGroup)
{
	struct Tlink *fPtr;
	int nResponse = 0;

	// From the start of the linked list (root), count the
	// number of times the topic of nKeyWordgroup has come up
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

		nResponse = (fPtr->nKeyWordGroup == nKeyWordGroup)? nResponse+1 : nResponse;
	}

	return (nResponse);

}	// end of GetTopicUsage

char *GetCategoryFromKeyGroup(int nKeyWordGroup)
{
	char szCountTemplate[]="SELECT * FROM categories WHERE category=%d";
	char szSQLString[50]="";
	static char szCategory[100] = "";
	int nResponse;

	sqlite3_stmt *stmt = NULL;

	// form the SQL string
	sprintf(szSQLString, szCountTemplate, nKeyWordGroup);

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(knowledgeDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
			return (0);

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	if (nResponse == SQLITE_ROW)
			strcpy(szCategory, (char *)sqlite3_column_text(stmt, CATEGORY_NAME));

	sqlite3_finalize(stmt);

	return (szCategory);

}	// end of GetCategoryFromKeyGroup

int CheckForProcessedCategories(int nKeyWordGroup)
{
	int i, nResponse = 0;

	for (i = TIME_HANDLER; i<END_MARKER; i++) {

		if ((nProcessCategories[i] == nKeyWordGroup)&& (i == TIME_HANDLER)) {
			// time handler routines
			nResponse = 1;
			break;
		}
		else if ((nProcessCategories[i] == nKeyWordGroup)&& (i == DATE_HANDLER)) {
			// date handler routines
			nResponse = 1;
			break;
		}
		else if ((nProcessCategories[i] == nKeyWordGroup)&& (i == DAY_HANDLER)) {
			// day handler routines
			nResponse = 1;
			break;
		}
		else if ((nProcessCategories[i] == nKeyWordGroup)&& (i == MONTH_HANDLER)) {
			// month handler routines
			nResponse = 1;
			break;
		}
		else if ((nProcessCategories[i] == nKeyWordGroup)&& (i == YEAR_HANDLER)) {
			// year handler routines
			nResponse = 1;
			break;
		}
	}

	return (nResponse);

}	// end of CheckForProcessedCategories

void GetResponseFromProcessGroup(int KeyWordGroup, char *szUserInput, JJGui *jibber_ui)
{
	char szTemplate[]="SELECT * FROM phrases WHERE category=%d";
	char szSQLString[50]="";
	char szCloser[10], szPhrase[250], szFinalResponse[512] = "";
	int nResponse, nRecordCount, nPosition, nLoop = 0;

	bool bFound = false;

	char szTimeStr[300]="";
	time_t t;
	struct tm *tmp;

	// get the time and date
	t = time(NULL);
	tmp = localtime(&t);

	sqlite3_stmt *stmt = NULL;

	// how many response phrases in this keyword group
	nRecordCount = GetPhrasesCount(KeyWordGroup);

	// which one did we use last time
	nPosition = nGroupPositionFlags[KeyWordGroup];

	// if we have never used a response from this group, randomly set the first one to use
	if (nPosition == (-2))
			nPosition = rand()% nRecordCount;
	else {
		++nPosition;
		nPosition = (nPosition >= nRecordCount)? 0 : nPosition;
	}

	if (bShow) {

		AppendText(jibber_ui, DIR_SYSTEM, "Group: %d, Resp: %d, Last: %d", KeyWordGroup, nRecordCount, nPosition);
	}

	// store the new position so that next time we know which one we used last
	nGroupPositionFlags[KeyWordGroup] = nPosition;

	// form the SQL string
	sprintf(szSQLString, szTemplate, KeyWordGroup);

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(knowledgeDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
			return;

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	// as long as we have a valid record set row
	while (nResponse == SQLITE_ROW) {

		// if this record row is one more than the one last used
		if (nLoop == nPosition) {

			bFound = true;

			// get phrase and the end punctuation - don't need the type
			strcpy(szPhrase, (char*)sqlite3_column_text(stmt, PHRASE));
			strcpy(szCloser, (char *)sqlite3_column_text(stmt, CLOSER));
		}

		// we found the response we were looking for
		if (bFound)
				nResponse = SQLITE_ERROR;
		else {

			++nLoop;
			// step through the loop to the next record
			nResponse = sqlite3_step(stmt);
		}
	}

	sqlite3_finalize(stmt);

	// format the time string, create the complete string and then display it
	strftime(szTimeStr, sizeof(szTimeStr), szPhrase, tmp);
	sprintf(szFinalResponse, "%s%s", szTimeStr, szCloser);
	SetToUppercase(szFinalResponse);
	AppendText(jibber_ui, DIR_OUT, szFinalResponse);

	// we don't need to record the time topics
	DeleteEndTopic();

}	// end of GetResponseFromProcessGroup

void PerformAboutCommand(JJGui *jibber_ui)
{
	char szSoftwareVersion[20];

	const char *Authors[] = {
		"Dave McKay <dmckay@btconnect.com>",
		NULL
	};

	const char *Catering[] = {
		"Pat MacKenzie <pcamackenzie@btconnect.com>",
		NULL
	};

	const char *Libraries[] = {
		"GCC https://gcc.gnu.org/",
		"GTK+ http://www.gtk.org/",
		"Glade https://glade.gnome.org/",
		"SQLite3 https://www.sqlite.org/",
		NULL
	};

	sprintf(szSoftwareVersion, "%d.%d.%d", JJ_VERSION_MAJOR, JJ_VERSION_MINOR, JJ_VERSION_BUILD);

	// set the icon
	GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file("jj_icon.png", NULL);

	// create the dialog box
	GtkWidget *AboutDialog = gtk_about_dialog_new();

	// set the data elements of the About box
	gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(AboutDialog), "Jibber Jabber");
	gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(AboutDialog), szSoftwareVersion);
	gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(AboutDialog), "\u00A9 Dave McKay 2013-18");
	gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(AboutDialog), "Chat-bot style conversation simulations.");
	gtk_about_dialog_set_website(GTK_ABOUT_DIALOG(AboutDialog), "http://www.ubuntujourneyman.com//");
	gtk_about_dialog_set_website_label(GTK_ABOUT_DIALOG(AboutDialog), "ubuntujourneyman.com");
	gtk_about_dialog_set_authors(GTK_ABOUT_DIALOG(AboutDialog), Authors);
	gtk_about_dialog_add_credit_section(GTK_ABOUT_DIALOG(AboutDialog), "Cups of Tea", Catering);
	gtk_about_dialog_add_credit_section(GTK_ABOUT_DIALOG(AboutDialog), "Libraries / Tools / APIs", Libraries);
	gtk_about_dialog_set_logo(GTK_ABOUT_DIALOG(AboutDialog), pixbuf);

	// don't need the handle to the icon anymore
	g_object_unref(pixbuf);
	pixbuf = NULL;

	// make the About dialog transient to the main window and a modal dialog
	gtk_window_set_modal((GtkWindow *)AboutDialog, TRUE);
	gtk_window_set_transient_for ((GtkWindow *)AboutDialog, (GtkWindow *)jibber_ui->window);

	// display the about box
	gtk_dialog_run(GTK_DIALOG(AboutDialog));

	// don't need the dialog box anymore
	gtk_widget_destroy(AboutDialog);

}	// end of PerformAboutCommand
