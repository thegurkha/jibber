// used to flag the commands in the parser
#define	COMMAND_TRON		0
#define	COMMAND_TROFF		1
#define	COMMAND_ABOUT		2
#define	COMMAND_HELP		3
#define	COMMAND_TOPICS		4
#define	COMMAND_SAVE		5
#define	COMMAND_QUIT		6
#define	COMMAND_EXIT		7


struct dotComm {

	char *Command;			// name of the command
	char *Abbrev;			// abbreviated form
	char *Help1;			// help text line 1 - displayed in Help and About
	char *Help2;			// help text line 2 - displayed in Help and About
	int nCommand;			// command number, commands are numbered sequentially

};

struct dotComm dCommands[] = {

	{
		".tron",
		".tron",
		".tron",
		"Turn system trace ON",
		COMMAND_TRON
	},

	{
		".troff",
		".troff",
		".troff",
		"Turn system trace OFF",
		COMMAND_TROFF
	},

	{
		".about",
		".a",
		".about",
		"Display an About box (also .a)",
		COMMAND_ABOUT
	},

	{
		".help",
		".h",
		".help",
		"Display this information (also .h)",
		COMMAND_HELP
	},

	{
		".topics",
		".t",
		".topics",
		"List of topics for this conversation (also .t)",
		COMMAND_TOPICS
	},

	{
		".save",
		".s",
		".save",
		"Save the current session as a text file (also .s)",
		COMMAND_SAVE
	},

	{
		".quit",
		".q",
		".quit",
		"Quit (exit) the program (also .q)",
		COMMAND_QUIT
	},

	{
		".exit",
		".x",
		".exit",
		"Same as .quit (also .x)",
		COMMAND_EXIT
	},

	{
		NULL,
		NULL,
		NULL,
		NULL,
		0
	}

};
