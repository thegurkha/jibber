/*
 * jibber_defines.h
 *
 * Copyright 2013 Dave McKay <dmckay@btconnect.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#define JJ_VERSION_MAJOR	1
#define JJ_VERSION_MINOR	2
#define JJ_VERSION_BUILD	0

// record fields for phrases table
#define TYPE							0
#define PHRASE						1
#define CLOSER						2

// record fields for category table
#define CATEGORY_NAME			0
#define CATEGORY_NUMBER		1

// record fields for triggers table
#define KEYPHRASE					0
#define CATEGORY 					1

// record fields for viewpoint table
#define FIRSTPERSON				0
#define THIRDPERSON				1

// record fields for postprocessing table
#define BADPAIRING				0
#define GOODPAIRING				1

// conversation direction
#define DIR_IN						0
#define DIR_OUT						1
#define DIR_SYSTEM				2
#define DIR_BLANK					3

// flags to set either silent or normal mode
#define NORMAL_MODE				0
#define SILENT_MODE				1

// defines for key word groups that require special processing for responses
#define	TIME_HANDLER			0
#define DATE_HANDLER			1
#define DAY_HANDLER				2
#define YEAR_HANDLER			3
#define MONTH_HANDLER			4
#define END_MARKER				5

#ifndef __ALREADY_DEFINED
#define __ALREADY_DEFINED

#if defined MAIN

typedef struct {

	GtkWidget	*window;
	GtkEntry	*edit_field;
	GtkWidget	*text_view;

} JJGui;

// JJ_GUI JJGui;

bool bShow;

// structure used in linked list of topics
typedef struct Tlink {

	char *TriggerPhrase;
	char *UserInput;
	char *CategoryName;

	int nKeyWordGroup;

	struct Tlink *Next;

} Tlink;

// pointers to the start and end of the linked list
struct Tlink *Root, *End;

#else

typedef struct {

	GtkWidget	*window;
	GtkEntry	*edit_field;
	GtkWidget	*text_view;

} JJGui;

// extern JJ_GUI JJGui;

// structure used in linked list of topics
typedef struct Tlink {

	char *TriggerPhrase;
	char *UserInput;
	char *CategoryName;

	int nKeyWordGroup;

	struct Tlink *Next;

} Tlink;

// pointers to the start and end of the linked list
extern Tlink *Root, *End;

extern bool bShow;

#endif	// MAIN

#endif	// __ALREADY_DEFINED
