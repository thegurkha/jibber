/*
 * parse_phrases.c
 *
 * Copyright 2013 Dave McKay dmckay@btconnect.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// structure used in linked list of topics
struct Tlink {
	char cFirstCharacter;
	char *Content;
	int	nKeyWordGroup;

	struct Tlink *Next;
};

// pointers to the start and end of the linked list
struct Tlink *Root, *End;

int ReadInPhrasesFile( char *szFilename );
struct Tlink *AddToList(int KeyWordGroup, char *Content);
void DoListWalk( void );

void WriteOutDumpFile( void );

void WriteOutPhrases( void );
void WriteOutTriggers( void );
void WriteOutViewpoints( void );
void WriteOutPostprocessing( void );
void WriteOutCategories( void );

#define STAND_ALONE							'0'
#define INTEGRATED							'1'

#define NULL_BYTE								0x00
#define RETURN									0x0D
#define NEW_LINE								0x0A
#define APOSTROPHE							0x27

#define HASH										'#'
#define SPACE										' '
#define NEW_KEYWORD							'='
#define NEW_GROUP_INDICATOR			'*'
#define NEW_SHIFT_INDICATOR			'@'
#define NEW_POSTP_INDICATOR			'!'
#define CATEGORY_TITLE					'$'

char *Line1="PRAGMA foreign_keys=OFF;";
char *Line2="BEGIN TRANSACTION;";
char *LineLast="COMMIT;";

char *CreatePhrasesTable="CREATE TABLE \"phrases\" (\n\t\"type\" TEXT,\n\t\"phrase\" TEXT,\n\t\"closer\" TEXT,\n\t\"category\" TEXT\n);";
char *CreateTriggersTable="CREATE TABLE \"triggers\" (\n\t\"keyphrase\" TEXT,\n\t\"category\" TEXT\n);";
char *CreateViewpointTable="CREATE TABLE \"viewpoint\" (\n\t\"firstperson\" TEXT,\n\t\"thirdperson\" TEXT\n);";
char *CreatePostprocessingTable="CREATE TABLE \"postprocessing\" (\n\t\"bad_pair\" TEXT,\n\t\"good_pair\" TEXT\n);";
char *CreateCategoriesTable="CREATE TABLE \"categories\" (\n\t\"category_name\" TEXT,\n\t\"category\" TEXT\n);";

// SQL output file handle
FILE *OutFile;

int main(int argc, char **argv)
{
	ReadInPhrasesFile( argv[1] );
	// DoListWalk();
	WriteOutDumpFile();

	return (0);

}	// end of main

int ReadInPhrasesFile( char *szFilename )
{
	char szBuffer[ 1024 ];
	int FirstCharacter, CurrentKeyWordGroup = 1;
	int nResponse=0;

	// file handle
	FILE *InFile;

	// open the file
	InFile=fopen(szFilename, "rt");

	// did it open OK
	if (InFile == NULL) {
		printf("Can't open %s file.\n", szFilename);
		return (1);
	}

    // read it line by line until we get to the end of the file
    while (fgets(szBuffer, 1023, InFile) != NULL) {

		// the first character of the line dictates what kind of line it is
		FirstCharacter = szBuffer[0];

		// if it is a comment ignore it
		if (FirstCharacter == HASH)
				continue;

		if (FirstCharacter == SPACE)
				continue;

		if (FirstCharacter == RETURN)
				continue;

		// if it isn't an asterisk, remove trailing line feeds etc
		if ( FirstCharacter != NEW_GROUP_INDICATOR )
				szBuffer[ strlen( szBuffer ) - 2 ] = NULL_BYTE;
		else {
			// increase the count of the keyword groups encountered so far
			CurrentKeyWordGroup++;
			continue;
		}

		// add this line to the linked list for later processing
		AddToList(CurrentKeyWordGroup, szBuffer);
	}

	// close the file
	fclose( InFile );

	return ( nResponse );

}   // ReadInPhrasesFile

struct Tlink *AddToList(int nKeyWordGroup, char *Content)
{
	struct Tlink *fPtr;
	int i, j;
	char szParsedContent[500];

	// no topics in the list yet
	if (Root == NULL) {

		// get memory for the new topic structure
		fPtr = (struct Tlink *) malloc(sizeof(struct Tlink));

		// can't allocate memory
		if (fPtr == NULL) {
			printf("Error: Can't allocate memory for topic: %s in linked list.", Content);
			return (NULL);
		}

		// set this topic as the start of the linked list
		Root = End = fPtr;
	}
	// topics exist, append new topic to the list
	else {
		// get memory for the new topic structure
		fPtr = (struct Tlink *) malloc(sizeof(struct Tlink));

		// can't allocate memory
		if (fPtr == NULL) {
			printf("Error: Can't allocate memory for topic: %s in linked list.", Content);
			return (NULL);
		}

		// point the hitherto last topic to the new last topic
		End->Next=fPtr;

		// this is now the end of the linked list
		End=fPtr;
	}

	// populate the fields
	fPtr->nKeyWordGroup = nKeyWordGroup;
	fPtr->cFirstCharacter = Content[0];

	// parse the content and escape apostrophes with another apostrophe. If the line is a phrases (response)
	// line we need to skip the semi-colon as well, otherwise we need to just skip the line type indicator
	i = (( fPtr->cFirstCharacter == INTEGRATED) || ( fPtr->cFirstCharacter == STAND_ALONE)) ? 2 : 1;

	for (j=0; Content[i] != NULL_BYTE; i++, j++ ) {

		if (Content[i] == APOSTROPHE) {
			szParsedContent[j++] = APOSTROPHE;
			szParsedContent[j] = APOSTROPHE;
		}
		else
				szParsedContent[j] = Content[i];
	}

	szParsedContent[j] = NULL_BYTE;

	fPtr->Content = strdup( szParsedContent );

	// can't allocate memory
	if (fPtr->Content == NULL) {
		printf("Error: Can't allocate memory for topic: %s in linked list.", Content);
		return (NULL);
	}

	// return from function
	return (fPtr);

}	// end of AddToList

void DoListWalk( void )
{
	struct Tlink *fPtr;

	// From the start of the linked list (root), display all topics
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

		printf("[%03d] '%c' (%02X), '%s'\n", fPtr->nKeyWordGroup, fPtr->cFirstCharacter, fPtr->cFirstCharacter, fPtr->Content );

	}

}	// end of DoListWalk

void WriteOutDumpFile( void )
{
	// open the file
	OutFile=fopen("worldview.sql", "w");

	// did it open OK
	if (OutFile == NULL) {
		printf("Can't open SQL dump file.\n");
		return;
	}

   fprintf(OutFile, "%s\n", Line1 );
   fprintf(OutFile, "%s\n\n", Line2 );
   fprintf(OutFile, "%s\n", CreatePhrasesTable );
   fprintf(OutFile, "%s\n", CreateTriggersTable );
   fprintf(OutFile, "%s\n", CreateViewpointTable );
   fprintf(OutFile, "%s\n", CreatePostprocessingTable );
   fprintf(OutFile, "%s\n\n", CreateCategoriesTable );

	// Each of these functions runs through the linked list looking for entries of a certain type
	// any that are found are written to OutFile as a SQL statement to populate the database
	WriteOutPhrases();
	WriteOutTriggers();
	WriteOutViewpoints();
	WriteOutPostprocessing();
	WriteOutCategories();

	// add the final COMMIT line
	fprintf(OutFile, "%s\n", LineLast );

	// close the OutFile
	fclose( OutFile );

}	// end of WriteOutDumpFile

void WriteOutPhrases( void )
{
	struct Tlink *fPtr;
	char szSeps[]=";\n";
	char *Content, *Closer;

	// From the start of the linked list (root), look for response entries
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

		if (( fPtr->cFirstCharacter == INTEGRATED) || ( fPtr->cFirstCharacter == STAND_ALONE)) {

			Content = strtok(fPtr->Content, szSeps);
			Closer = strtok(NULL, szSeps);

			printf("INSERT INTO \"phrases\" VALUES('%c','%s','%s','%d');\n", fPtr->cFirstCharacter, Content, Closer, fPtr->nKeyWordGroup );
			fprintf(OutFile, "INSERT INTO \"phrases\" VALUES('%c','%s','%s','%d');\n", fPtr->cFirstCharacter, Content, Closer, fPtr->nKeyWordGroup );
		}
	}

	fprintf(OutFile, "\n");

}	// end of WriteOutPhrases

void WriteOutTriggers( void )
{
	struct Tlink *fPtr;

	// From the start of the linked list (root), look for keyword entries
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

		if ( fPtr->cFirstCharacter == NEW_KEYWORD ) {

			printf("INSERT INTO \"triggers\" VALUES('%s','%d');\n", fPtr->Content, fPtr->nKeyWordGroup );
			fprintf(OutFile, "INSERT INTO \"triggers\" VALUES('%s','%d');\n", fPtr->Content, fPtr->nKeyWordGroup );
		}
	}

	fprintf(OutFile, "\n");

}	// end of WriteOutTriggers

void WriteOutViewpoints( void )
{
	struct Tlink *fPtr;
	char szSeps[]=",\n";
	char *First, *Second;

	// From the start of the linked list (root), look for Viewpoint shift entries
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

		if ( fPtr->cFirstCharacter == NEW_SHIFT_INDICATOR ) {

			First = strtok(fPtr->Content, szSeps);
			Second = strtok(NULL, szSeps);

			printf("INSERT INTO \"viewpoint\" VALUES('%s','%s');\n", First, Second );
			fprintf(OutFile, "INSERT INTO \"viewpoint\" VALUES('%s','%s');\n", First, Second );
		}
	}

	fprintf(OutFile, "\n");

}	// end of WriteOutViewpoints

void WriteOutPostprocessing( void )
{
	struct Tlink *fPtr;
	char szSeps[]=",\n";
	char *First, *Second;

	// From the start of the linked list (root), look for postprocessing entries
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

		if ( fPtr->cFirstCharacter == NEW_POSTP_INDICATOR ) {

			First = strtok(fPtr->Content, szSeps);
			Second = strtok(NULL, szSeps);

			printf("INSERT INTO \"postprocessing\" VALUES('%s','%s');\n", First, Second );
			fprintf(OutFile, "INSERT INTO \"postprocessing\" VALUES('%s','%s');\n", First, Second );
		}
	}

	fprintf(OutFile, "\n");

}	// end of WriteOutPostprocessing

void WriteOutCategories( void )
{
	struct Tlink *fPtr;

	// From the start of the linked list (root), look for categories entries
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

		if ( fPtr->cFirstCharacter == CATEGORY_TITLE ) {

			printf("INSERT INTO \"categories\" VALUES('%s','%d');\n", fPtr->Content, fPtr->nKeyWordGroup );
			fprintf(OutFile, "INSERT INTO \"categories\" VALUES('%s','%d');\n", fPtr->Content, fPtr->nKeyWordGroup );
		}
	}

}	// end of WriteOutCategories
